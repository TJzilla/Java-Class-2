
import java.util.ArrayList;

public class Suitcase {

    private final int maxWeight;
    private int totalWeight;
    private ArrayList<Thing> things;
    private Thing heaviest;

    public Suitcase(int maxWeight) {
        this.maxWeight = maxWeight;
        this.totalWeight = 0;
        this.heaviest = null;
        this.things = new ArrayList<Thing>();
    }

    public void addThing(Thing thing) {
        if (this.totalWeight + thing.getWeight() <= maxWeight) {
            this.things.add(thing);
            this.totalWeight += thing.getWeight();
        }
        
        if (heaviest == null || thing.getWeight() > this.heaviest.getWeight()) {
            this.heaviest = thing;
        }
    }

    public void printThings() {
        for (Thing list : this.things) {
            System.out.println(list);
        }
    }

    public int totalWeight() {
        return this.totalWeight;
    }
    
    public Thing heaviestThing() {
        return this.heaviest;
    }

    @Override
    public String toString() {
        if (this.things.size() == 0) {
            return "empty" + "(" + this.totalWeight + " kg)";
        } else if (this.things.size() == 1) {
            return this.things.size() + " thing (" + this.totalWeight + " kg)";
        }
        return this.things.size() + " things (" + this.totalWeight + " kg)";
    }
}
