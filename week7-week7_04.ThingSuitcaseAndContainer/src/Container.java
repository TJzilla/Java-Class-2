
import java.util.ArrayList;

public class Container {

    private final int maxWeight;
    private int totalWeight;
    private ArrayList<Suitcase> suitcases;

    public Container(int maxWeight) {
        this.maxWeight = maxWeight;
        this.totalWeight = 0;
        this.suitcases = new ArrayList<Suitcase>();
    }

    public void addSuitcase(Suitcase suitcase) {
        if (this.totalWeight + suitcase.totalWeight() <= this.maxWeight) {
            this.suitcases.add(suitcase);
            this.totalWeight += suitcase.totalWeight();
        }

    }

    @Override
    public String toString() {
        if (this.suitcases.size() == 0) {
            return "empty" + "(" + this.totalWeight + " kg)";
        } else if (this.suitcases.size() == 1) {
            return this.suitcases.size() + " suitcase (" + this.totalWeight + " kg)";
        }
        return this.suitcases.size() + " suitcases (" + this.totalWeight + " kg)";
    }
    
    public void printThings() {
        for (Suitcase list : this.suitcases) {
            list.printThings();
        }
    }
}
