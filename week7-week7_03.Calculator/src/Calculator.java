
public class Calculator {
    
    private Reader reader;
    private int calcsDone;
    
    public Calculator() {
        this.reader = new Reader();
        this.calcsDone = 0;
    }
    
     public void start() {
        while (true) {
            System.out.print("command: ");
            String command = reader.readString();
            if (command.equals("end")) {
                break;
            }

            if (command.equals("sum")) {
                sum();
                this.calcsDone++;
            } else if (command.equals("difference")) {
                difference();
                this.calcsDone++;
            } else if (command.equals("product")) {
                product();
                this.calcsDone++;
            }
        }

        statistics();
    }

    private void sum() {
        System.out.print("value1: ");
        int value1 = this.reader.readInteger();
        
        System.out.print("value2: ");
        int value2 = this.reader.readInteger();
        
        System.out.println("sum of the values " + (value1 + value2));
    }

    private void difference() {
       System.out.print("value1: ");
        int value1 = this.reader.readInteger();
        
        System.out.print("value2: ");
        int value2 = this.reader.readInteger();
        
        System.out.println("difference of the values " + (value1 - value2));
    }

    private void product() {
        System.out.print("value1: ");
        int value1 = this.reader.readInteger();
        
        System.out.print("value2: ");
        int value2 = this.reader.readInteger();
        
        System.out.println("product of the values " + (value1 * value2));
    }

    private void statistics() {
        System.out.println("Calculations done " + this.calcsDone);
    }
}
