
import java.util.ArrayList;


public class Changer {
    private ArrayList<Change> test;
     public Changer() {
         this.test = new ArrayList<Change>();
     }
     
     public void addChange(Change change) {
         test.add(change);
     }
     
     public String change(String characterString) {
         for(Change list : test) {
             characterString = list.change(characterString);
         }
         return characterString;
     }
}
