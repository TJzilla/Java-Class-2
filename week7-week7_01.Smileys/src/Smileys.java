
public class Smileys {

    public static void main(String[] args) {
        // Test your method at least with the following
        // printWithSmileys("Method");
        // printWithSmileys("Beerbottle");
        // printWithSmileys("Interface");\
        printWithSmileys("\\:D/");

    }

    private static void printWithSmileys(String characterString) {
        boolean isOdd = characterString.length() % 2 == 1;
        int smileyLength;

        if (!isOdd) {
            smileyLength = (characterString.length() + 6) / 2;
            printSmiley(smileyLength);
            System.out.println("");
            printSmiley(1);
            System.out.print(" " + characterString + " ");
            printSmiley(1);
            System.out.println("");
            printSmiley(smileyLength);

        } else if (isOdd) {
            smileyLength = (characterString.length() + 7) / 2;
            printSmiley(smileyLength);
            System.out.println("");
            printSmiley(1);
            System.out.print(" " + characterString + "  ");
            printSmiley(1);
            System.out.println("");
            printSmiley(smileyLength);
        }
    }
    
    

    

    private static void printSmiley(int numberOfTimes) {
        while (numberOfTimes > 0) {
            System.out.print(":)");
            numberOfTimes--;
        }

    }

}
